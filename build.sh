ARCH=$1

rm /etc/apt/sources.list.d/nodesource.list*

apt-get update

ARCH=$1
MACHINE=$2
HOST=$(gcc -dumpmachine)
apt-get update
apt-get install pkg-config devscripts checkinstall libgl1-mesa-dev:$ARCH libglu-dev:$ARCH libasound2-dev:$ARCH libpulse-dev:$ARCH libudev-dev:$ARCH yasm:$ARCH libsdl2-2.0-0:$ARCH libsdl2-dev:$ARCH libudev-dev:$ARCH libudev1:$ARCH libglib2.0-0:$ARCH libglib2.0-dev:$ARCH libegl1-mesa:$ARCH libegl1-mesa-dev:$ARCH libgles2-mesa-dev:$ARCH libmirclient-dev:$ARCH libmirclient*:$ARCH libxkbcommon-dev:$ARCH libvpx-dev:$ARCH libvpx3:$ARCH libpng16-dev:$ARCH libpng16-16:$ARCH libvorbis-dev:$ARCH libvorbis0a:$ARCH libwayland-client0:$ARCH libwayland-cursor0:$ARCH libwayland-dev:$ARCH libwayland-server0:$ARCH libmirwayland0:$ARCH libmirwayland-dev:$ARCH libxkbcommon-dev:$ARCH libxkbcommon0:$ARCH wayland-protocols -y
apt-get install debhelper:$ARCH autoconf:$ARCH dh-autoconf:$ARCH dh-make dh-autoreconf:$ARCH fcitx-libs-dev libibus-1.0-dev -y

apt-get build-dep libsdl2:$ARCH

export PKG_CONFIG_PATH="/usr/lib/${MACHINE}/pkgconfig":"/usr/lib/pkgconfig":"/usr/share/pkgconfig"

echo $HOST
echo $ARCH
echo $MACHINE

ls -al /usr/lib/*/pkgconfig
ls -al /usr/lib/pkgconfig
ls -al /usr/share/pkgconfig

for value in wayland-client wayland-scanner wayland-protocols wayland-egl wayland-cursor egl xkbcommon mirclient
do
    echo $value
    if pkg-config --exists $value ; then
        echo "present";
    else
        echo "absent";
    fi
done


dpkg-buildpackage -us -uc -d
mkdir build
mv ../libsdl2*.deb build/
